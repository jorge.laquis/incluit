module.exports.handler = (event, context, callback) => {
  const dataEncoded = JSON.parse(event.body).encode.split(".");
  let response = {
    headers: JSON.parse(Buffer.from(dataEncoded[0], 'base64')),
    body: JSON.parse(Buffer.from(dataEncoded[1], 'base64'))
  };
  console.log(response)
  
  callback(null, JSON.stringify(response));
};