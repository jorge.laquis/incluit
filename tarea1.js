let students = [
    {
      name: 'JORGE',
      lastname: 'LAQUIS',
      score: 8
    },
    {
      name: 'JOHN',
      lastname: 'DOE',
      score: 4
    },
    {
      name: 'JAMES',
      lastname: 'SHAW',
      score: 2
    }
  ];

let i = 0;
let minimunNote = 4;
let resultFor = [];
let resultForEach = [];
let resultWhile = [];
  
for (let index = 0; index < students.length; index++) {
    if (students[index].score >= minimunNote) {
        students[index].name = capitalize(students[index].name);
        students[index].lastname = capitalize(students[index].lastname);
        resultFor.push(students[index]);
    }
};
resultFor.sort( (a, b) => (a.score > b.score) ? 1 : -1);
console.log(resultFor);

students.forEach(element => {
    if (element.score >= minimunNote) {
        element.name = capitalize(element.name);
        element.lastname = capitalize(element.lastname);
        resultForEach.push(element);
    }
});
resultForEach.sort( (a, b) => (a.score > b.score) ? 1 : -1);
console.log(resultForEach);

while(i < students.length){
    if (students[i].score >= minimunNote) {
        students[i].name = capitalize(students[i].name);
        students[i].lastname = capitalize(students[i].lastname);
        resultWhile.push(students[i]);
    }
    i++;
};
resultWhile.sort( (a, b) => (a.score > b.score) ? 1 : -1);
console.log(resultWhile);

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};