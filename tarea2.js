const students = require('./db/students.json');
let result = [];
let minimunNote = 4;

async function secondsDelay(callback) {
    await sleep(2500);
    console.log("The approved students are:");
    callback(students)
        .then(() => {
            console.log("Okey, resolved!");
        })
        .catch(() => {
            console.log("Something went wrong!");
        })
};

async function approvedStudents(payload) {
    return new Promise((resolve) => {
        payload.forEach(element => {
            if (element.score >= minimunNote) {
                element.name = capitalize(element.name);
                element.lastname = capitalize(element.lastname);
                result.push(element);
            }
        }); resolve(result);
    })
    .then((data) => {
        sorting(data);
        console.log(data);
    })
    .catch(() => {
        console.log("Error!");
    })
};
  
function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    })
};

function sorting(payload) {
    payload.sort( (a, b) => (a.score > b.score) ? 1 : -1);
};

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};

secondsDelay(approvedStudents);